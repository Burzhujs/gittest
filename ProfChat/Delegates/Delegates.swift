//
//  Delegates.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 26/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation

protocol ObjectsSelectedDelegate: class {
    func saveSelectedObjects(groups: [ChatGroup])
}
