//
//  TextResponseTableViewCell.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 15/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//


import UIKit

class TextResponseTableViewCell: UITableViewCell {
    static let ReuseIdentifier: String = "TextResponseTableViewCell"

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var labelContainerView: UIView!
    @IBOutlet weak var timeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        iconImageView.layer.cornerRadius = 16
        iconImageView.layer.masksToBounds = true
        
        labelContainerView.layer.cornerRadius = 8
        labelContainerView.layer.masksToBounds = true
        
        labelContainerView.backgroundColor = .clear
        labelContainerView.layer.borderColor = UIColor.blue.cgColor
        labelContainerView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadData(with message: ChatMessage) {
        
        //iconImageView.image = message.image
        messageLabel.text = message.text
        messageLabel.textColor = .gray
        
        let dateFormatterMessage = DateFormatter()
        dateFormatterMessage.setLocalizedDateFormatFromTemplate("hh:mm")
        timeLabel.text = dateFormatterMessage.string(from: message.date)
        timeLabel.textColor = .gray
        
    }
}
