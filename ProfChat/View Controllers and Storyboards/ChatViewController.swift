//
//  ChatViewController.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 19/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import UIKit

class ChatViewController: UIViewController {
    
    var group: ChatGroup!
    @IBOutlet weak var tableView: UITableView!
    
    var messages = [ChatMessage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationItem.title = group.title
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 30))
        self.tableView.backgroundColor = Colors.BACKGROUND_COLOR
        self.view.backgroundColor = Colors.BACKGROUND_COLOR
        
        let userNib = UINib(nibName: TextResponseTableViewCell.ReuseIdentifier, bundle: nil)
        tableView.register(userNib, forCellReuseIdentifier: TextResponseTableViewCell.ReuseIdentifier)
        
        let adminNib = UINib(nibName: UserTableViewCell.ReuseIdentifier, bundle: nil)
        tableView.register(adminNib, forCellReuseIdentifier: UserTableViewCell.ReuseIdentifier)
        
        sendWelcomeMessage()
        sendWelcomeMessage()
        sendWelcomeMessage()
    }
    
    func sendMessage(_ message: ChatMessage) {
        messages.append(message)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: messages.count - 1, section: 0)], with: .automatic)
        tableView.endUpdates()
        
        let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    func sendWelcomeMessage() {
        let firstTime = true
        if firstTime {
            let text = "Oh hello again Jon. I hope you’ve been well. What would you like to hear?"
            let message = ChatMessage(date: Date(), type: .admin, message: text)
            messages.append(message)
        }
    }
    
}

extension ChatViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        
        switch message.type {
        case .user:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
            cell.loadData(with: message)
            return cell
        case .admin:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextResponseTableViewCell", for: indexPath) as! TextResponseTableViewCell
            cell.loadData(with: message)
            return cell
        }
    }
}
