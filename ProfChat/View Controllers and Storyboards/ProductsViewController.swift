//
//  ProductsViewController.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 28/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import UIKit

class ProductsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var searchController = UISearchController(searchResultsController: nil)

    var results: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Colors.BACKGROUND_COLOR
        let nib = UINib(nibName: ObjectsTableViewCell.ReuseIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ObjectsTableViewCell.ReuseIdentifier)
        
        setUpSearchBar()

        
    }
    
    private func setUpSearchBar() {
        /*self.definesPresentationContext = true
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.scopeButtonTitles = ["Visi", "Betons", "Naglas", "Jumti", "Koks", "Reģipsis", "Logi"]
        searchController.searchBar.showsScopeBar = true
        searchController.searchBar.delegate = self
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.barTintColor = UIColor.white*/
        
        searchController.obscuresBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        searchController.searchResultsUpdater = self
        searchController.searchBar.scopeButtonTitles = ["All", "Sweet", "Fresh", "Other"]
        searchController.searchBar.delegate = self
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.barTintColor = UIColor.white
    }
    
}

extension ProductsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: ObjectsTableViewCell.ReuseIdentifier, for: indexPath) as? ObjectsTableViewCell
            else {
                fatalError("DequeueReusableCell failed while casting")
        }
        
        let result = results[indexPath.row]
        cell.loadData(object: ChatGroup(), isLast: result == results.last)
        
        return cell
    }
    
    
    
    
}

extension ProductsViewController: UISearchBarDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentsforSearchText(searchText: searchController.searchBar.text!, scope: scope)

    }

    func filterContentsforSearchText(searchText: String, scope: String = "All"){
        /*filteredFruits = fruits.filter { fruit in
            let categoryMatch = (scope == "All") || (fruit.category == scope)
            if searchText.isEmpty {
                return categoryMatch
            }
            return categoryMatch && fruit.name.lowercased().contains(searchText.lowercased())
        }*/

        tableView.reloadData()
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentsforSearchText(searchText: searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])

        print("selectedScope: \(selectedScope)")
        if selectedScope == 1{
            print("Sweet")
        }

    }
    
    
    
    
}
