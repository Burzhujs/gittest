//
//  MainPageViewController.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 15/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import UIKit
import Foundation

class MainPageViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var groups: [ChatGroup] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        groups = DataHelper.getAllChatGroups()
        tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        addNavigationItems()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 30))
        self.tableView.backgroundColor = Colors.BACKGROUND_COLOR
        self.view.backgroundColor = Colors.BACKGROUND_COLOR
        
        let nib = UINib(nibName: MainPageTableViewCell.ReuseIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: MainPageTableViewCell.ReuseIdentifier)
    }

    fileprivate func addNavigationItems() {
        self.navigationItem.title = "Grupas"
        
        let rightButtonImage = UIImage(named: "Profile")
        let rightButton = UIBarButtonItem(image: rightButtonImage,
                                          style: .plain,
                                          target: self,
                                          action: #selector(self.openProfileAction))
        self.navigationItem.rightBarButtonItem = rightButton

    }
    
    @objc func openProfileAction() {
        self.performSegue(withIdentifier: "OpenProfile", sender: nil)
    }
}

extension MainPageViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: MainPageTableViewCell.ReuseIdentifier, for: indexPath) as? MainPageTableViewCell
            else {
                fatalError("DequeueReusableCell failed while casting")
        }
        
        let group = groups[indexPath.row]
        cell.loadData(group: group, isLast: group == groups.last)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "OpenChat", sender: indexPath.row)
    }
    
    
}
