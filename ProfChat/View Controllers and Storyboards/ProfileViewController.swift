//
//  ProfileViewController.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 22/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewController: UIViewController {
        
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var objectsLabel: UILabel!
    @IBOutlet weak var objectsArrow: UIImageView!
    @IBOutlet weak var supplierSwitch: UISwitch!
    @IBOutlet weak var saveChangesButton: UIButton!
    
    var objectsViewController: ObjectsTableViewController?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(DataHelper.getAllChatGroups().first?.isSelected)
        //self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //self.navigationItem.setHidesBackButton(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.BACKGROUND_COLOR
        decorateSaveChangesButton()
        addObjectsInteractions()
    }
    
    fileprivate func decorateSaveChangesButton() {
        saveChangesButton.backgroundColor = Colors.BLUE_NAVIGATION_COLOR
        saveChangesButton.layer.cornerRadius = 5.0
        saveChangesButton.layer.borderColor = Colors.BLUE_NAVIGATION_COLOR.cgColor
        saveChangesButton.layer.borderWidth = 1.0
    }
    
    fileprivate func addObjectsInteractions() {
        objectsLabel.isUserInteractionEnabled = true
        objectsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openObjectsAction)))
        
        objectsArrow.isUserInteractionEnabled = true
        objectsArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openObjectsAction)))
    }
    
    @objc func openObjectsAction() {
        objectsViewController = ObjectsTableViewController()
        objectsViewController?.delegate = self
        objectsViewController!.modalPresentationStyle = .overCurrentContext
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        present(objectsViewController!, animated: true, completion: nil)
    }
}

extension ProfileViewController: ObjectsSelectedDelegate {
    func saveSelectedObjects(groups: [ChatGroup]) {
        objectsViewController = nil
        
        DataHelper.updateChatGroups(groups: groups)
        
    }
    
}
