//
//  User.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 18/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import CoreData

class User: NSObject {
    
    var id: String!
    var name: String!
    var chatGroupID: [String] = []
    
    convenience init(id: String, name: String, chatGroupID: [String]) {
        self.init()
        
        self.id = id
        self.name = name
        self.chatGroupID = chatGroupID
    }
    
    convenience init (dbItem: NSManagedObject, context: NSManagedObjectContext) {
        self.init()
        
        context.performAndWait {
            
            if let id = dbItem.value(forKey: "id") as? String {
                self.id = id
                self.name = dbItem.value(forKey: "name") as? String
                self.chatGroupID = dbItem.value(forKey: "chatGroupID") as? [String] ?? []
            }
        }
    }
    
}
