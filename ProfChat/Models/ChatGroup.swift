//
//  ChatGroup.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 15/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import CoreData

class ChatGroup: NSObject {
    
    var id: String!
    var title: String!
    var isSelected: Bool = true
    var participants: [User] = []
    
    convenience init(id: String, title: String) {
        self.init()
        
        self.id = id
        self.title = title
    }
    
    convenience init (dbItem: NSManagedObject, context: NSManagedObjectContext) {
        self.init()
        
        context.performAndWait {
            
            if let id = dbItem.value(forKey: "id") as? String {
                self.id = id
                self.title = dbItem.value(forKey: "title") as? String
                self.isSelected = dbItem.value(forKey: "isSelected") as! Bool
                self.participants = DataHelper.getParticipantsByID(id: id)
            }
        }
    }
    
}
