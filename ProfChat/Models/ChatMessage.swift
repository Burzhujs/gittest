//
//  ChatMessage.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 19/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import UIKit

enum MessageType {
    case user
    case admin
}

class ChatMessage {
    
    var text: String = ""
    var date: Date
    var type: MessageType
    var image: UIImage!
    
    init(date: Date, type: MessageType, message: String) {
        self.date = date
        self.type = type
        self.text = message
        self.image = self.getImage()
    }
    
    func getImage() -> UIImage {
        switch self.type {
        case .user:
            return UIImage(named: "User") ?? UIImage()
        default:
            return UIImage(named: "Admin") ?? UIImage()
        }
    }
}
