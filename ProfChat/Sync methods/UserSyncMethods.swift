//
//  UserSyncMethods.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 18/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import CoreData

extension DataHelper {
        
    static func insertUserInDB(_ user: User) {
        let coreDataStack = CoreDataStack()
        if let context = coreDataStack.newPrivateQueueContext() {
                
            context.performAndWait {
                let entity =  NSEntityDescription.entity(forEntityName: "User", in: context)
                let userDB = NSManagedObject(entity: entity!, insertInto: context)
                
                userDB.setValue(user.id, forKey: "id")
                userDB.setValue(user.name, forKey: "name")
                userDB.setValue(user.chatGroupID, forKey: "chatGroupID")
            }
            
            do {
                coreDataStack.saveContexts(context: context)
            }
        }
    }
    
    static func getAllUsers() -> [User] {
        var result = [User]()
        
        let coreDataStack = CoreDataStack()
        if let context = coreDataStack.newPrivateQueueContext() {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
            
            context.performAndWait {
                do {
                    if let results = try context.fetch(fetchRequest) as? [NSManagedObject] {
                        for item in results {
                            if let _ = item.value(forKey: "id") as? String {
                                let user = User(dbItem: item, context: context)
                                result.append(user)
                            }
                        }
                    }
                    
                } catch let error as NSError {
                    print("Could not fetch \(error), \(error.userInfo)")
                    
                }
            }

            return result
        } else {
            return []
        }
    }
    
    static func getParticipantsByID(id: String) -> [User] {
        var result = [User]()
        let coreDataStack = CoreDataStack()
        if let context = coreDataStack.newPrivateQueueContext() {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")

            context.performAndWait {
                    do {
                        if let results = try context.fetch(fetchRequest) as? [NSManagedObject] {
                            for item in results {
                                if let _ = item.value(forKey: "id") as? String {
                                    let user = User(dbItem: item, context: context)
                                    if user.chatGroupID.contains(id) {
                                        result.append(user)
                                    }
                                }
                            }
                        }
                        
                    } catch let error as NSError {
                        print("Could not fetch \(error), \(error.userInfo)")
                        
                    }
                }

                return result
            } else {
                return []
            }
        }
            
}
