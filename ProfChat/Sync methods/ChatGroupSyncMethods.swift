//
//  ChatGroupSyncMethods.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 18/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import CoreData

extension DataHelper {
    
    static func insertChatGroupInDB(_ chatGroup: ChatGroup) {
        let coreDataStack = CoreDataStack()
        if let context = coreDataStack.newPrivateQueueContext() {
            context.performAndWait {
                let entity =  NSEntityDescription.entity(forEntityName: "ChatGroup", in: context)
                let chatGroupDB = NSManagedObject(entity: entity!, insertInto: context)
                
                chatGroupDB.setValue(chatGroup.id, forKey: "id")
                chatGroupDB.setValue(chatGroup.title, forKey: "title")
                chatGroupDB.setValue(chatGroup.isSelected, forKey: "isSelected")
            }
            
            do {
                coreDataStack.saveContexts(context: context)
            }
        }
    }
    
    static func updateChatGroups(groups: [ChatGroup]) {
        let coreDataStack = CoreDataStack()
        if let context = coreDataStack.newPrivateQueueContext() {
            context.performAndWait {
                for group in groups {
                    if let groupDB = DataHelper.getChatGroupByID(group.id, context: context) {
                        groupDB.setValue(group.isSelected, forKey: "isSelected")
                    }
                }
                
            }
            
            do {
                coreDataStack.saveContexts(context: context)
            }
        }
    }
    
    static func getAllChatGroups(forSettings: Bool = false) -> [ChatGroup] {
        var result = [ChatGroup]()
        
        let coreDataStack = CoreDataStack()
        if let context = coreDataStack.newPrivateQueueContext() {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatGroup")
            if !forSettings {
                fetchRequest.predicate = NSPredicate(format: "isSelected = %@", NSNumber(value: true))

            }

            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
            
            context.performAndWait {
                do {
                    if let results = try context.fetch(fetchRequest) as? [NSManagedObject] {
                        for item in results {
                            if let _ = item.value(forKey: "id") as? String {
                                let chatGroup = ChatGroup(dbItem: item, context: context)
                                result.append(chatGroup)
                            }
                        }
                    }
                    
                } catch let error as NSError {
                    print("Could not fetch \(error), \(error.userInfo)")
                    
                }
            }

            return result
        } else {
            return []
        }
    }
    
    private static func getChatGroupByID(_ id: String, context: NSManagedObjectContext) -> NSManagedObject? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ChatGroup")
        fetchRequest.predicate = NSPredicate(format: "id = %@ ", id )
        var results: [Any]? = nil
        
        context.performAndWait {
            do {
                results = try context.fetch(fetchRequest)
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
            
        }
        
        if let result = results?.first as? NSManagedObject {
            return result
        } else {
            return nil
        }
    }
    
}
