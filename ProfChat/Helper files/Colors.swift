//
//  Colors.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 19/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import UIKit

class Colors {

    static let BACKGROUND_COLOR: UIColor = UIColor(hex: "FCF6F5")
    static let BLUE_NAVIGATION_COLOR: UIColor = UIColor(hex: "84A7E3")
    static let BLUE_SYSTEM_COLOR: UIColor = UIColor.systemBlue
}
