//
//  CoreDataStack.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 18/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    func newPrivateQueueContext() -> NSManagedObjectContext? {
        let parentContext = self.mainQueueContext
        
        if parentContext == nil {
            return nil
        }
        
        let privateQueueContext =
            NSManagedObjectContext(concurrencyType:
                .privateQueueConcurrencyType)
        privateQueueContext.parent = parentContext
        privateQueueContext.name = "private"
        privateQueueContext.mergePolicy =
        NSMergeByPropertyObjectTrumpMergePolicy
        return privateQueueContext
    }
    
    lazy var mainQueueContext: NSManagedObjectContext? = {
        let parentContext = self.masterContext
        
        if parentContext == nil {
            return nil
        }
        
        var mainQueueContext =
            NSManagedObjectContext(concurrencyType:
                .mainQueueConcurrencyType)
        mainQueueContext.parent = parentContext
        mainQueueContext.name = "main"
        mainQueueContext.mergePolicy =
        NSMergeByPropertyObjectTrumpMergePolicy
        return mainQueueContext
    }()
    
    private lazy var masterContext: NSManagedObjectContext? = {
        let coordinator = self.persistentStoreCoordinator

        var masterContext =
            NSManagedObjectContext(concurrencyType:
                .privateQueueConcurrencyType)
        masterContext.persistentStoreCoordinator = coordinator
        masterContext.name = "master"
        masterContext.mergePolicy =
        NSMergeByPropertyObjectTrumpMergePolicy
        return masterContext
    }()
    
    // MARK: - Setting up Core Data stack

    private lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "ProfChat", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true])
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    private lazy var applicationDocumentsDirectory: NSURL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()

    func saveContexts(context: NSManagedObjectContext) {
        context.performAndWait {
            do {
                if context.hasChanges {
                    try context.save()
                }
            } catch let error as NSError {
                print("Could not save \(error), \(error.userInfo)")
            }
            
        }
        context.parent?.performAndWait {
            do {
                if (context.parent?.hasChanges)! {
                    try context.parent?.save()
                }
            } catch let error as NSError {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
        context.parent?.parent?.performAndWait {
            do {
                if (context.parent?.parent?.hasChanges)! {
                    try context.parent?.parent?.save()
                }
            } catch let error as NSError {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
}
