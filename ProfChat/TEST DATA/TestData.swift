//
//  TestData.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 18/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation

class TestData {
    
    static func createTestUsers() -> [User] {
        let user1: User = User(id: "1", name: "Jānis", chatGroupID: ["1", "2", "3", "4", "5"])
        let user2: User = User(id: "2", name: "Pēteris", chatGroupID: ["1", "2", "3", "4"])
        let user3: User = User(id: "3", name: "Matīss", chatGroupID: ["1", "2", "3"])
        let user4: User = User(id: "4", name: "Juris", chatGroupID: ["1", "4", "5"])
        let user5: User = User(id: "5", name: "Edgars", chatGroupID: ["1", "2", "3", "4", "5"])

        return [user1, user2, user3, user4, user5]
    }
    
    static func createTestGroups() -> [ChatGroup] {
        let group1: ChatGroup = ChatGroup(id: "1", title: "Objekts nr 1")
        let group2: ChatGroup = ChatGroup(id: "2", title: "Objekts nr 2")
        let group3: ChatGroup = ChatGroup(id: "3", title: "Objekts nr 3")
        let group4: ChatGroup = ChatGroup(id: "4", title: "Objekts nr 4")
        let group5: ChatGroup = ChatGroup(id: "5", title: "Objekts nr 5")
        
        return [group1, group2, group3, group4, group5]
    }
    
}
