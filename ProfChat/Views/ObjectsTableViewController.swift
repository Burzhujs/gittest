//
//  ObjectsTableViewController.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 22/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import Foundation
import UIKit


class ObjectsTableViewController: UIViewController {
    
    var tableView: UITableView!
    var headerView: UIView!
    var backButton: UIButton!
    var titleLabel: UILabel!
    
    
    let groups = DataHelper.getAllChatGroups(forSettings: true)
    weak var delegate: ObjectsSelectedDelegate?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Colors.BACKGROUND_COLOR
        tableView.allowsMultipleSelection = true
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        
        headerView = UIView()
        headerView.backgroundColor = Colors.BACKGROUND_COLOR
        headerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(headerView)
        
        backButton = UIButton()
        backButton.setTitle("Atpakaļ", for: UIControl.State())
        backButton.titleLabel?.textAlignment = .right
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        backButton.setTitleColor(Colors.BLUE_SYSTEM_COLOR, for: UIControl.State())
        backButton.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(backButton)
        
        titleLabel = UILabel()
        titleLabel.text = "Objekti"
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(titleLabel)
        
        self.view.addConstraint(NSLayoutConstraint(item: tableView as UITableView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        self.view.addConstraint(NSLayoutConstraint(item: tableView as UITableView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 300))
        var constraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[tableView]-0-|", options: [], metrics: nil, views: ["tableView": tableView as UITableView])
        NSLayoutConstraint.activate(constraint)
        
        constraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[headerView]-0-|", options: [], metrics: nil, views: ["headerView": headerView as UIView])
        NSLayoutConstraint.activate(constraint)
        
        constraint = NSLayoutConstraint.constraints(withVisualFormat: "V:[headerView(==40)]-0-[tableView]", options: [], metrics: nil, views: ["headerView": headerView as UIView, "tableView": tableView as UITableView])
        NSLayoutConstraint.activate(constraint)
        
        constraint = NSLayoutConstraint.constraints(withVisualFormat: "H:[backButton(==100)]-5-|", options: [], metrics: nil, views: ["backButton": backButton as UIButton])
        NSLayoutConstraint.activate(constraint)
        
        self.view.addConstraint(NSLayoutConstraint(item: backButton as UIButton, attribute: .centerY, relatedBy: .equal, toItem: headerView as UIView, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        self.view.addConstraint(NSLayoutConstraint(item: backButton as UIButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 25))
        
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel as UILabel, attribute: .centerY, relatedBy: .equal, toItem: headerView as UIView, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel as UILabel, attribute: .centerX, relatedBy: .equal, toItem: headerView as UIView, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel as UILabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 25))
        self.view.addConstraint(NSLayoutConstraint(item: titleLabel as UILabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80))
        
        let nib = UINib(nibName: ObjectsTableViewCell.ReuseIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ObjectsTableViewCell.ReuseIdentifier)
        
    }
    
    @objc func backAction() {
        if let delegate = self.delegate {
            delegate.saveSelectedObjects(groups: groups)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension ObjectsTableViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: ObjectsTableViewCell.ReuseIdentifier, for: indexPath) as? ObjectsTableViewCell
            else {
                fatalError("DequeueReusableCell failed while casting")
        }
        
        let group = groups[indexPath.row]
        cell.loadData(object: group, isLast: group == groups.last)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let group = groups[indexPath.row]
        if group.isSelected {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        groups[indexPath.row].isSelected = !groups[indexPath.row].isSelected
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        groups[indexPath.row].isSelected = !groups[indexPath.row].isSelected
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}
