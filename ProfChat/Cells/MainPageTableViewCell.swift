//
//  MainPageTableViewCell.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 18/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import UIKit

class MainPageTableViewCell: UITableViewCell {
    static let ReuseIdentifier: String = "MainPageTableViewCell"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var participantsLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(group: ChatGroup, isLast: Bool) {
        self.setTexts(group: group)
        separatorView.isHidden = isLast
    }
    
    private func setTexts(group: ChatGroup) {
        titleLabel.text = group.title
        
        var participantsLabelText = "Dalībnieki: "
        
        for user in group.participants {
            participantsLabelText += user.name
            if !(user == group.participants.last) {
                participantsLabelText += ", "
            }
             
        }
        
        participantsLabel.text = participantsLabelText
    }
    
}
