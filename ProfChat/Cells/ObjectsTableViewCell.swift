//
//  ObjectsTableViewCell.swift
//  ProfChat
//
//  Created by Matiss Mamedovs on 22/05/2020.
//  Copyright © 2020 Divi Group. All rights reserved.
//

import UIKit

class ObjectsTableViewCell: UITableViewCell {
    static let ReuseIdentifier: String = "ObjectsTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        selectedBackgroundView = backgroundView
            
        if selected {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
        }
    }
    
    func loadData(object: ChatGroup, isLast: Bool) {
        self.titleLabel.text = object.title
        separatorView.isHidden = isLast
    }
    
}
